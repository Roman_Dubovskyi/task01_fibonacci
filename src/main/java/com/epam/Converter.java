package com.epam;

/**
 * This class is used to convert double value
 * to percent in program.
 */
final class Converter {

    private Converter() {
    }

    /**
     * This field stores constant value '10000', that
     * is used for conversion.
     */
    private static final int PERCENTAGE_CONVERT_1 = 10000;
    /**
     * This field stores constant value '100', that
     * is used for conversion.
     */
    private static final int PERCENTAGE_CONVERT_2 = 100;

    /**
     * This method converts double value into percent.
     * It's used in printPercentage method
     *
     * @param toConvert takes a value that needs to be converted
     * @return returns converted double value
     * @see Fibonacci#printPercentage()
     */
    static double convert(final double toConvert) {
        int toConvertInt = (int) (toConvert * PERCENTAGE_CONVERT_1);
        return ((double) toConvertInt) / PERCENTAGE_CONVERT_2;
    }
}
