package com.epam;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * This Class is used to perform actions with interval
 * of numbers. User will input first and last number of
 * interval and program will print separately Ods and Evens.
 * Then program will show sum of Odd and Even numbers.
 */
class Interval {
    /**
     * This is instance of class "Scanner"
     * for program to read user's command.
     */
    private Scanner sc = new Scanner(System.in, "UTF-8");
    /**
     * Field to store sum of Odd numbers in the interval.
     */
    private int sumOfOds;
    /**
     * Field to store sum of Even numbers in the interval.
     */
    private int sumOfEvens;
    /**
     * Field to store all the numbers of interval that user
     * will create.
     */
    private ArrayList<Integer> interval = new ArrayList<Integer>();

    /**
     * This method will allow user to input first and last number
     * of interval. Then it will add all the numbers in between
     * using 'for' loop into ArrayList 'interval'.
     */
    void initializeInterval() {
        System.out.println("Please enter first and last number of interval");
        int startOfInterval = sc.nextInt();
        int endOfInterval = sc.nextInt();
        for (int i = 0; i <= (endOfInterval - startOfInterval); i++) {
            interval.add(i + startOfInterval);
        }
        System.out.println("Your interval is: " + interval);
    }

    /**
     * This method will print only Odd numbers of the interval
     * from start to end and Even numbers from end to
     * start using 'for each' loop.
     */
    void printOddsAndEvens() {
        System.out.println("Odd numbers from start to end:");
        for (Integer integer : interval) {
            if (integer % 2 != 0) {
                System.out.print(integer + " ");
            }
        }
        System.out.println();
        System.out.println("Even numbers from end to start: ");
        for (int i = interval.size() - 1; i >= 0; i--) {
            if (interval.get(i) % 2 == 0) {
                System.out.print(interval.get(i) + " ");
            }
        }
        System.out.println();
    }

    /**
     * This method will print sum of Odd and
     * Even numbers from the interval, checking it
     * by 'for' loop.
     */
    void printSumOfOdsAndEvens() {
        System.out.println("Sum of Ods is: ");
        for (Integer integer : interval) {
            if (integer % 2 != 0) {
                sumOfOds += integer;
            }
        }
        System.out.println(sumOfOds);
        System.out.println("Sum of Evens is: ");
        for (int i = interval.size() - 1; i >= 0; i--) {
            if (interval.get(i) % 2 == 0) {
                sumOfEvens += interval.get(i);
            }
        }
        System.out.println(sumOfEvens);
    }
}
