package com.epam;

import java.util.Scanner;

/**
 * This class is used to create a menu
 * for user, where he will be able to
 * interact with program.
 */
class Menu {
    /**
     * This is instance of class "Scanner"
     * for program to read user's command.
     */
    private static Scanner sc = new Scanner(System.in, "UTF-8");
    /**
     * This is instance of class Fibonacci
     * to have access to its methods.
     * @see  Fibonacci
     */
    private Fibonacci fib = new Fibonacci();
    /**
     * This is instance of class Interval
     * to have access ot its methods.
     * @see Interval
     */
    private Interval interval = new Interval();

    /**
     * This method is used to allow user
     * to user to choose what action should
     * program perform: work with interval of numbers,
     * create Fibonacci sequence, or exit.
     */
    void useMenu() {
        createMenu();
        int choice = sc.nextInt();
        switch (choice) {
            case 1:
                interval.initializeInterval();
                interval.printOddsAndEvens();
                interval.printSumOfOdsAndEvens();
                useMenu();
                break;
            case 2:
                fib.buildFibonacci();
                fib.printFibonacci();
                fib.printPercentage();
                useMenu();
                break;
            case 0:
                Runtime.getRuntime().exit(0);
                break;
            default:
                System.out.println("Incorrect input, try again");
                useMenu();
        }
    }

    /**
     * This method is used to print for
     * user options that he can use.
     */
    private void createMenu() {
        System.out.println("1. Create and work with interval of numbers.");
        System.out.println("2. Create Fibonacci sequence.");
        System.out.println("0. Exit.");
    }
}
