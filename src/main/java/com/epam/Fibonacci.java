package com.epam;

import java.util.Scanner;

/**
 * This class is used to perform actions with Fibonacci sequence.
 * User can insert number of sets and program will print
 * last number of sequence, biggest Odd and biggest Even number.
 * Then it will print percentage of Ods and Evens of the sequence.
 */
class Fibonacci {
    /**
     * This is instance of class "Scanner"
     * for program to read user's command.
     */
    private static Scanner sc = new Scanner(System.in, "UTF-8");
    /**
     * This field stores numbers of sets in sequence
     * that is entered by user.
     */
    private int nOfFibonacci;
    /**
     * This field stores the biggest Even number
     * in the sequence.
     */
    private int biggestEvenFibonacci;
    /**
     * This field stores the biggest Odd number
     * in the sequence.
     */
    private int biggestOddFibonacci;
    /**
     * This field stores amount of Odd numbers
     * int the sequence.
     */
    private double countOfOdsFib;
    /**
     * This field stores amount of Even numbers
     * int the sequence.
     */
    private double countOfEvensFib;

    /**
     * This method allows user to enter size
     * of sequence using 'Scanner'.
     */
    void buildFibonacci() {
        System.out.println("Now we will create Fibonacci numbers");
        System.out.println("Please enter size of set 'N': ");
        nOfFibonacci = sc.nextInt();

    }

    /**
     * This method builds Fibonacci sequence using recursion.
     * @param n size of sequence entered by user in 'buildFibonacci' method
     * @see Fibonacci#buildFibonacci()
     * @return last number of sequence
     */
    private int calculateFibonacci(final int n) {
        if ((n == 1) || (n == 0)) {
            return n;
        } else {
            return calculateFibonacci(n - 1) + calculateFibonacci(n - 2);
        }
    }

    /**
     * This method prints last element of the sequence.
     * Then calculates and print max Odd and Even number of the sequence.
     */
    void printFibonacci() {
        int resultOfFibonacci = calculateFibonacci(nOfFibonacci);
        System.out.println("Last element of sequence: " + resultOfFibonacci);
        System.out.println("Max Even is: ");
        if (resultOfFibonacci % 2 == 0) {
            biggestEvenFibonacci = resultOfFibonacci;
        } else {
            for (int i = nOfFibonacci - 1; i >= 0; i--) {
                if (calculateFibonacci(i) % 2 == 0) {
                    biggestEvenFibonacci = calculateFibonacci(i);
                    break;
                }
            }
        }
        System.out.println(biggestEvenFibonacci);
        System.out.println("Max Odd is: ");
        if (resultOfFibonacci % 2 != 0) {
            biggestOddFibonacci = resultOfFibonacci;
        } else {
            for (int i = nOfFibonacci - 1; i >= 0; i--) {
                if (calculateFibonacci(i) % 2 != 0) {
                    biggestOddFibonacci = calculateFibonacci(i);
                    break;
                }
            }
        }
        System.out.println(biggestOddFibonacci);
    }

    /**
     * This method calculates and prints percentage of
     * Odd and Even numbers in the sequence.
     * It uses converter to display info user friendly.
     * @see Converter#convert(double)
     */
    void printPercentage() {
        for (int i = 1; i <= nOfFibonacci; i++) {
            if (calculateFibonacci(i) % 2 == 0) {
                countOfEvensFib++;
            } else {
                countOfOdsFib++;
            }
        }
        double percentageOdsFib = countOfOdsFib / nOfFibonacci;
        double percentageOdsFormat = Converter.convert(percentageOdsFib);
        System.out.println("Percentage of Ods: " + percentageOdsFormat + "%");
        double percentageEvensFib = countOfEvensFib / nOfFibonacci;
        double percentEvensFormat = Converter.convert(percentageEvensFib);
        System.out.println("Percentage of Evens: " + percentEvensFormat + "%");
    }
}
