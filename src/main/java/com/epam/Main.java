package com.epam;

/**
 * This is main class of the program.
 *
 * @author Roman Dubovskyi
 * @version 1.0
 */
public final class Main {

    private Main() {
    }

    /**
     * That is start of the program.
     *
     * @param args command line values
     */
    public static void main(final String[] args) {
        /* Prints greeting to user. Standard output. */
        System.out.println("Welcome to the program!");
        Menu menu = new Menu();
        menu.useMenu();
    }
}
